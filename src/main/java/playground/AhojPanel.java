package playground;

import javax.swing.*;
import java.awt.*;

public class AhojPanel extends JPanel
{
    public AhojPanel()
    {
        setLayout(new BorderLayout());

        JButton vlevoButton = new JButton("Vlevo");
        vlevoButton.setBackground(Color.yellow);
        add(vlevoButton, BorderLayout.WEST);

        add(new JButton("Vpravo"), BorderLayout.EAST);
        add(new JButton("Nahoře"), BorderLayout.NORTH);
        add(new JButton("Dole"), BorderLayout.SOUTH);

    }
}
