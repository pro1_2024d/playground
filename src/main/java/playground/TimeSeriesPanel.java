package playground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class TimeSeriesPanel extends JPanel
{
    public TimeSeriesPanel()
    {
        JToolBar toolBar = new JToolBar();
        TextField countTextField = new TextField();
        JButton setupButton = new JButton("Nastavit");
        JButton exportButton = new JButton("Export");

        JPanel flowPanel = new JPanel();
        flowPanel.setLayout(new FlowLayout());

        toolBar.add(countTextField);
        toolBar.add(setupButton);
        toolBar.add(exportButton);

        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.NORTH);
        add(flowPanel, BorderLayout.CENTER);

        List<TextField> fieldsList = new ArrayList<>();

        setupButton.addActionListener((e)->{
            int count = Integer.parseInt(countTextField.getText());
            for(int i=0; i<count; i++)
            {
                TextField f = new TextField();
                flowPanel.add(f);
                fieldsList.add(f);
            }
        });

        exportButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(TextField f : fieldsList)
                {
                    System.out.println(f.getText());
                }
            }
        });



    }
}
