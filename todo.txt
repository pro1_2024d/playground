﻿TODO 1
- V záložce s hlavičkou "Kreslení" se budou pohybem myši kreslit kolečka stejně jako v ukázce "Úvod do GUI / GuiSamples.zip"

TODO 2
- Přidejte záložku s hlavičkou "Světové strany".
- Bude zde jeden panel se čtyřmi velkými tlačítky zarovnanými pomocí BorderLayout.
- Horní tlačítko bude mít modré pozadí, pravé tlačítko bude mít červené pozadí, dolní tlačítko bude mít zelené pozadí, levé tlačítko bude mít žluté pozadí.

TODO 3
- Přidejte záložku s hlavičkou "Časová řada".
- Obsah záložky bude pomocí BorderLayout rozdělený na dvě části:
  - JToolBar obsahující textové pole pro zadání čísla, tlačítko "Nastavit", tlačítko "Export"
  - Panel s rozložením FlowLayout, kde po kliknutí na "Nastavit" vznikne zadaný počet prvků JTextField
- Po kliknutí na "Export" se obsah všech prvků JTextField přepíše do konzole (System.out.println)

TODO 4
- Přidejte záložku s hlavičkou "Náhodná čísla". 
- Záložka bude obsahovat panel s rozložením BoxLayout.
- Nápověda:
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    JPanel p = new JPanel();
    p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
- Tento panel bude nejprve obsahovat jedno tlačítko.
- Po každém kliknutí na tlačítko přibyde do panelu další potomek typu JLabel, který zobrazí náhodné číslo mezi 0 a 100.
- Nápověda:
    Po přidání prvku volat revalidate().
- Text v JLabel by měl mít font Courier, tučný, velikost 30.
- Upravte obsah záložky tak, aby panel obsahující prvky JLabel byl obsažen v prvku JScrollPane. Díky tomu bude viditelný, i když naroste přes velikost okna.
- Nápověda:
    JScrollPane scrollPane = new JScrollPane(p);

TODO 5
- Přidejte záložku s hlavičkou "Jména".
- Bude zde textové pole (JTextField) obsahující seznam osob.
- Dále zde bude tlačítko "Přidat".
- Po kliknutí se zobrazí dialog s textovým polem a tlačítkem "OK".
- Po kliknutí na OK se obsah textového pole v dialogu přidá do seznamu osob.
- Podobně funguje dialog v příkladu "Úvod do GUI / Osobní finance" (pozor, hotový kód tam není ke stažení)
